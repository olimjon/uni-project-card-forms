/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package normukhamedov_11616969_assignment3_task3;

import java.util.ArrayList;
import java.util.Collections;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 *
 * @author Helkern
 */
public class Normukhamedov_11616969_Assignment3_task3 extends Application {

    @Override
    public void start(Stage primaryStage) {
        HBox hbox = addHBox(); //storing return value in hbox/vbox class object
        VBox vbox = addVBox();

        StackPane root = new StackPane();//setting root a container
        Scene scene = new Scene(root, 500, 500);//setting size of the container

        root.getChildren().addAll(hbox, vbox);//append objects to the root pane object

        primaryStage.setTitle("Cards"); //Title of the program window
        primaryStage.setScene(scene); //setting scene as a main
        primaryStage.show();//finally showing
    }

    public HBox addHBox() {
        HBox hbox = new HBox();//new object declaration
        hbox.setPadding(new Insets(15)); //setting padding for all sides
        hbox.setAlignment(Pos.TOP_CENTER);//top center align the pane
        hbox.setSpacing(10);//space between each image box
        hbox.setStyle("-fx-background-color: #000000;"); //setting bg color

        ArrayList<Integer> cardList = new ArrayList<Integer>(); //Using list of integers is one methods to make sure output numbers will be unique as for the requirements
        for (int i = 1; i <= 52; i++) { //Adding numbers from 1 - 52inc into the arraylist of integers. 
            cardList.add(new Integer(i));
        }
        Collections.shuffle(cardList);//randomly permuting the list using collection's default source of randomness
        // load the images into each new Image objects
        
        Image card1 = new Image("file:card/" + cardList.get(0) + ".png");
        Image card2 = new Image("file:card/" + cardList.get(1) + ".png");
        Image card3 = new Image("file:card/" + cardList.get(2) + ".png");
        // simple displays ImageView the image as is
        ImageView cardView1 = new ImageView();
        ImageView cardView2 = new ImageView();
        ImageView cardView3 = new ImageView();
        cardView1.setImage(card1);//setting image to the cardview
        cardView2.setImage(card2);
        cardView3.setImage(card3);

        hbox.getChildren().addAll(cardView1, cardView2, cardView3); // add carview collection to the hbox

        return hbox;//return all hbox coll
    }

    public VBox addVBox(){
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(8);
        
        Image card1 = new Image("file:card/53.png");
        Image card2 = new Image("file:card/54.png");
        ImageView cardView1 = new ImageView();
        ImageView cardView2 = new ImageView();
        cardView1.setRotate(45);//rotate the cardview
        cardView2.setRotate(90);
        cardView1.setImage(card1);
        cardView2.setImage(card2);
        
        vbox.getChildren().addAll(cardView1, cardView2);
        
        return vbox;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);//loads the launch method
    }

}
